#include <Ultrasonic.h>
#include <Servo.h>

// Configurações do Servo Motor
Servo servo;
int servoPin = 13;
int minPos = 70;
int maxPos = 30; 

// Configurações do botão
int buttonPin = 12;

// Configurações do ultrasonic
int echoPin = 8;
int triggerPin = 9;
int distanciaCM;


// Tempo que o registro ficará aberto
int segundos;

// Inicia o sensor
Ultrasonic ultrasonic(triggerPin, echoPin);

void setup() {
  // Define o botao como INPUT
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
  
  // Anexa o Servo Motor ao pino utilizado
  servo.attach(servoPin);

  // Inicia o motor sempre no mesmo ponto
  servo.write(minPos);
  delay(1000);
}
 
void loop() {
  // Verifica se o bota para abrir o registro foi pressionado
  if (digitalRead(buttonPin) == HIGH) {
    // Realiza a leitura do sensor ultrasonic
    distanciaCM = ultrasonic.read();
    // Gera o tempo em que o registro será deixado aberto
    // O tempo gerado é baseado na distancia retornada do ultrasonic
    segundos = map(distanciaCM,0, 30, 8000, 10000);

    // Exibindo as informações para verificar os valores
    Serial.print("Distancia: ");
    Serial.println(distanciaCM);
    Serial.print("Segundos: ");
    Serial.println(segundos);

    // Para o sistema para não ter problema com os clicks conscutivos do botao
    delay(700);

    abrirRegistro();

    // Para o sistema pelo tempo determinado anteriormente
    delay(segundos);

    fecharRegistro();
  }
}

void abrirRegistro() {
  // Abre o registro até o ponto máximo definido
  servo.write(maxPos);
}

void fecharRegistro() {
  // Fecha o registro no ponto definido
  servo.write(minPos);
}
