# Bem vindo ao projeto Default Farm

Temos como objetivo melhorias na fazenda Progresso.

Projeto integrador do 1º semestre de 2019

## Equipe

* Adanny (3º termo)
* Carlos (3º termo)
* Davi (3º termo)
* Micael (3º termo)
* Renan (3º termo)
* Weverton (3º termo)

# Wiki

Conheça a nossa [wiki](https://gitlab.com/BDAg/progressfarm/wikis/home).
